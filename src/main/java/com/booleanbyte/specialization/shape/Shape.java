package com.booleanbyte.specialization.shape;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.github.davidmoten.rtree2.geometry.Geometry;

import javafx.scene.canvas.GraphicsContext;

public interface Shape {
	
	/**
	 * Calculates the signed distance to the shape from point p.
	 * 
	 * @param p The point to evaluate the shape distance for.
	 * @return the signed distance to the shape at point p.
	 */
	public double sdf(Vector2D p);

	/**
	 * Calculates the height of a shape at point p.
	 * 
	 * @param p The point to evaluate the shape height for.
	 * @return the height at point p.
	 */
	public double height(Vector2D p);

	/**
	 * Draw simplified shape in 2D on a JavaFX canvas using GraphicsContext. The
	 * graphics context is transformed by the preview renderer calling this method,
	 * so the shape should be painted using the true coordinates of the shape
	 * without any translation or rotation transforms by this method.
	 * 
	 * @param g
	 */
	public void paint(GraphicsContext g);
	
	/**
	 * Collects all planar points and their shape heights in the provided map. It is
	 * assumed that all duplicated planar points have the same height and they can
	 * therefore treated as equals.
	 * 
	 * @param pointsCollection
	 */
	public void collectPoints(Map<Vector2D, Double> pointsCollection);
	
	public void collect2dVerticies(Collection<Vector2D> verticies);
	
	public void collect3dVerticies(Collection<Vector3D> verticies);
	
	public Geometry getBoundGeometry(double offset);
}
