package com.booleanbyte.specialization.worldsynth.module.attributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.booleanbyte.specialization.feature.Attribute;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeAttributes;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleAttributesCollect extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeAttributes requestData = (DatatypeAttributes) request.data;
		
		//----------READ INPUTS----------//
		
		//Check if both inputs are available
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary
		List<Attribute> primary = ((DatatypeAttributes) inputs.get("primary")).getAttributes();
		List<Attribute> secondary = ((DatatypeAttributes) inputs.get("secondary")).getAttributes();
		
		//----------BUILD----------//
		
		List<Attribute> out = new ArrayList<Attribute>(primary);
		out.addAll(secondary);
		
		requestData.setAttributes(out);
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Collect";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeAttributes(), "Primary"),
				new ModuleInput(new DatatypeAttributes(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeAttributes(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

}
