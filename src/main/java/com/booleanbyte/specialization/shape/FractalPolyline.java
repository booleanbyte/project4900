package com.booleanbyte.specialization.shape;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class FractalPolyline extends Polyline {
	
	protected final double mpdFactor;
	protected final int mpdIterations;
	
	private static List<Vector2D> mpdPolyline(Vector2D c1, Vector2D c2, double displacementFactor, int iterations) {
		Random r = new Random(c1.hashCode() ^ c2.hashCode());
		
		List<Vector2D> vertices = new LinkedList<Vector2D>();
		vertices.add(c1);
		vertices.add(c2);
		
		// Midpoint displacement
		for (int i = 1; i <= iterations; i++) {
			int j = 0;
			while (j < vertices.size()-1) {
				Vector2D v1 = vertices.get(j);
				Vector2D v2 = vertices.get((j+1)%vertices.size());
				Vector2D dir = v2.subtract(v1);
				Vector2D norm = new Vector2D(-dir.getY(), dir.getX()).normalize();
				
				double displacement = (r.nextDouble()*2.0-1.0)*dir.getNorm()*displacementFactor;
				Vector2D v15 = v1.add(dir.scalarMultiply(0.5)).add(norm.scalarMultiply(displacement));
				
				vertices.add(++j, v15);
				j++;
			}
		}
		
		return vertices;
	}
	
	
	
	public FractalPolyline(Vector3D c1, Vector3D c2, double mpdFactor, int mpdIterations) {
		super(mpdPolyline(new Vector2D(c1.getX(), c1.getY()), new Vector2D(c2.getX(), c2.getY()), mpdFactor, mpdIterations), c1.getZ(), c2.getZ());
		
		this.mpdFactor = mpdFactor;
		this.mpdIterations = mpdIterations;
	}
	
//	@Override
//	public Rectangle getBoundGeometry(double offset) {
//		double l = c1.distance(c2);
//		l = l * (1 - Math.pow(mpdFactor, mpdIterations+1))/(1 - mpdFactor) - l;
//		offset += l;
//		return Geometries.rectangle(
//				Math.min(c1.getX() - offset, c2.getX() - offset),
//				Math.min(c1.getY() - offset, c2.getY() - offset),
//				Math.max(c1.getX() + offset, c2.getX() + offset),
//				Math.max(c1.getY() + offset, c2.getY() + offset));
//	}
}
