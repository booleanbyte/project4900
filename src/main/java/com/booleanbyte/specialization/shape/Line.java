package com.booleanbyte.specialization.shape;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Rectangle;

import javafx.scene.canvas.GraphicsContext;

public class Line implements Shape, ConnectingPointsShape {
	
	final Vector2D c1, c2;
	final Double h1, h2;
	
	public Line(double x1, double y1, double height1, double x2, double y2, double height2) {
		this(new Vector2D(x1, y1), height1, new Vector2D(x2, y2), height2);
	}
	
	public Line(Vector2D c1, double height1, Vector2D c2, double height2) {
		this.c1 = c1;
		this.c2 = c2;
		this.h1 = height1;
		this.h2 = height2;
	}
	
	public Line(Vector3D c1, Vector3D c2) {
		this.c1 = new Vector2D(c1.getX(), c1.getY());
		this.c2 = new Vector2D(c2.getX(), c2.getY());
		this.h1 = c1.getZ();
		this.h2 = c2.getZ();
	}
	
	@Override
	public boolean areConnected(Vector2D p1, Vector2D p2) {
		return (c1.equals(p1) && c2.equals(p2)) || (c1.equals(p2) && c2.equals(p1));
	}
	
	@Override
	public double sdf(Vector2D p) {
		Vector2D pa = p.subtract(c1);
		Vector2D ba = c2.subtract(c1);
		double h = pa.dotProduct(ba) / ba.dotProduct(ba);
		h = Math.min(Math.max(h, 0.0), 1.0);
		return pa.subtract(h, ba).getNorm();
	}
	
	@Override
	public double height(Vector2D p) {
		Vector2D pa = p.subtract(c1);
		Vector2D ba = c2.subtract(c1);
		double h = pa.dotProduct(ba) / ba.dotProduct(ba);
		h = Math.min(Math.max(h, 0.0), 1.0);
		return lerp(h1, h2, h);
	}
	
	/**
	 * Linear interpolation from a to b where t is in the range [0, 1].
	 * 
	 * <p>
	 * {@code a + t * (b - a)}
	 * 
	 * @param a
	 * @param b
	 * @param t
	 * @return
	 */
	private final double lerp(double a, double b, double t) {
		return a + t * (b - a);
	}

	@Override
	public void paint(GraphicsContext g) {
		g.strokeLine(c1.getX(), c1.getY(), c2.getX(), c2.getY());
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		pointsCollection.put(c1, h1);
		pointsCollection.put(c2, h2);
	}
	
	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		verticies.add(c1);
		verticies.add(c2);
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		verticies.add(new Vector3D(c1.getX(), c1.getY(), h1));
		verticies.add(new Vector3D(c2.getX(), c2.getY(), h2));
	}

	@Override
	public Rectangle getBoundGeometry(double offset) {
		return Geometries.rectangle(
				Math.min(c1.getX() - offset, c2.getX() - offset),
				Math.min(c1.getY() - offset, c2.getY() - offset),
				Math.max(c1.getX() + offset, c2.getX() + offset),
				Math.max(c1.getY() + offset, c2.getY() + offset));
	}
}
