package com.booleanbyte.specialization.worldsynth.module;

import net.worldsynth.module.IModuleCategory;

import javafx.scene.paint.Color;

public enum SpecializationModuleCategory implements IModuleCategory {
	VALIDATOR {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 100);
		}
	}
}
