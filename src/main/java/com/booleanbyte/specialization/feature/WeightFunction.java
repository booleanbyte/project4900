package com.booleanbyte.specialization.feature;

import java.util.function.Function;

public abstract class WeightFunction implements Function<Double, Double> {
	
	private static final double a = 200.0;
	private static final double b = 8.0;
	
	public static final Function<Double, Double> W1 = w1(a);
	public static final Function<Double, Double> W2 = w2(a);
	public static final Function<Double, Double> W3 = w3(a, b);
	
	
	public static final WeightFunction w1(double a) {
		return new WeightFunction() {
			@Override
			public Double apply(Double d) {
				d = Math.max(0.0, d);
				double da = d/a;
				return Math.max(0, 1-da);
			}
			
			@Override
			public double influenceDistance() {
				return a;
			}
			
			@Override
			public String toString() {
				return "W1";
			}
		};
	}
	
	public static final WeightFunction w2(double a) {
		return new WeightFunction() {
			@Override
			public Double apply(Double d) {
				d = Math.max(0.0, d);
				double da = d/a;
				return Math.max(0, (1-da)/d);
			}
			
			@Override
			public double influenceDistance() {
				return a;
			}
			
			@Override
			public String toString() {
				return "W2";
			}
		};
	}
	
	public static final WeightFunction w3(double a, double b) {
		return new WeightFunction() {
			@Override
			public Double apply(Double d) {
				d = Math.max(0.0, d);
				double da = d/a;
				double db = d/b;
				return Math.max(0, (1-da*da*da*da)/(db*db+1));
			}
			
			@Override
			public double influenceDistance() {
				return a;
			}
			
			@Override
			public String toString() {
				return "W3";
			}
		};
	}
	
	public abstract double influenceDistance();
}
