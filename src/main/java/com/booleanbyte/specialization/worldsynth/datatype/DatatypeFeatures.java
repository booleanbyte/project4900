package com.booleanbyte.specialization.worldsynth.datatype;

import java.util.ArrayList;
import java.util.List;

import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.worldsynth.patcher.ui.preview.FeaturesSimplifiedRender;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class DatatypeFeatures extends AbstractDatatype {
	
	private List<ShapedFeature> features;
	
	public List<ShapedFeature> getFeatures() {
		return features;
	}
	
	public void setFeatures(List<ShapedFeature> features) {
		this.features = features;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.BLUE;
	}

	@Override
	public String getDatatypeName() {
		return "Features";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeatures df = new DatatypeFeatures();
		if (features != null) df.features = new ArrayList<ShapedFeature>(features);
		return df;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeatures();
	}
	
	@Override
	public AbstractPreviewRender getPreviewRender() {
//		return new FeaturesSdfRender();
		return new FeaturesSimplifiedRender();
	}
}
