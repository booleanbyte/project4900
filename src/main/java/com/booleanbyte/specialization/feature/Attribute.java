package com.booleanbyte.specialization.feature;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public interface Attribute {

	/**
	 * Add the contribution of the attribute through the appropriate interface in
	 * the merger.
	 * 
	 * @param merger the merger instance for the point being evaluated.
	 * @param the    feature the attribute is being evaluated for.
	 * @param p      the vector pointing to the point in the plane being evaluated.
	 * @param sdf    the SDF distance to the feature parent feature being evaluated.
	 */
	public void contrubute(Merger merger, ShapedFeature featur, Vector2D p, double sdf);

	/**
	 * Get the influence distance from the feature geometry the attribute covers.
	 * 
	 * @return the influence distance
	 */
	public double influenceDistance();
}
