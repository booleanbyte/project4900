package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.shape.Line;
import com.booleanbyte.specialization.shape.Polygon;
import com.booleanbyte.specialization.shape.Shape;
import com.booleanbyte.specialization.shape.Shapes;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;
import com.booleanbyte.specialization.worldsynth.module.SpecializationModuleCategory;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleFeatureConflictRemoval extends AbstractModule {
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Low priority"), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("High priority"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//
		
		//Check if both inputs are available
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary
		List<ShapedFeature> primary = ((DatatypeFeatures) inputs.get("primary")).getFeatures();
		List<ShapedFeature> secondary = ((DatatypeFeatures) inputs.get("secondary")).getFeatures();
		
		//----------BUILD----------//
		
		List<ShapedFeature> out = new ArrayList<ShapedFeature>();
		
		for (ShapedFeature s1: primary) {
			boolean conflict = false;
			for (ShapedFeature s2: secondary) {
				if (conflict(s1.getShape(), s2.getShape())) {
					conflict = true;
					break;
				}
			}
			
			if (!conflict) {
				out.add(s1);
			}
		}
		
		requestData.setFeatures(out);
		return requestData;
	}
	
	private boolean conflict(Shape shape1, Shape shape2) {
		if (shape1 instanceof Line && shape2 instanceof Line) {
			return Shapes.lineLineConflict((Line) shape1, (Line) shape2); 
		}
		else if (shape1 instanceof Polygon && shape2 instanceof Line) {
			return Shapes.polygonLineConflict((Polygon) shape1, (Line) shape2);
		}
		else if (shape1 instanceof Line && shape2 instanceof Polygon) {
			return Shapes.polygonLineConflict((Polygon) shape2, (Line) shape1);
		}
		return false;
	}

	@Override
	public String getModuleName() {
		return "Conflict removal";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return SpecializationModuleCategory.VALIDATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Low priority"),
				new ModuleInput(new DatatypeFeatures(), "High priority")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
