package com.booleanbyte.specialization.shape;

import java.util.LinkedList;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class DynamicFractalPolyline extends FractalPolyline {
	
	public DynamicFractalPolyline(Vector3D c1, Vector3D c2, double mpdFactor, int mpdIterations) {
		super(c1, c2, mpdFactor, mpdIterations);
	}
	
	@Override
	public double sdf(Vector2D p) {
		double d = Double.POSITIVE_INFINITY; // Result distance
		
		LinkedList<LineTuple> lines = new LinkedList<LineTuple>();
		LinkedList<Double> dists = new LinkedList<Double>();
		
		lines.add(new LineTuple(0, polyline.length-1)); // Start with root line
		
		while (lines.size() > 0) {
			double d_ = Double.POSITIVE_INFINITY;
			int size = lines.size();
			
			for (int j = 0; j < size; j++) {
				LineTuple lt = lines.get(j);
				double dd = lineSdf(p, lt);
				dists.add(dd);
				d_ = Math.min(d_, dd);
			}
			
			for(int j = 0; j < size; j++) {
				LineTuple lt = lines.remove();
				double dd = dists.remove();
				
				if (!lt.canSplit()) {
					// If the line segment is at max depth, apply for minimum distance of final result.
					d = Math.min(d, dd);
					continue;
				}
				
				double ll = lineLength(lt); // Line length
				double maxPDisp = ll * mpdFactor; // Max estimated distance change after displacement
				double minPDist = dd - maxPDisp; // Minimum estimated possible distance after displacement
				
				if (minPDist <= Math.min(d, d_)) {
					// If the distance may become closer than the current closest, split the line.
					lines.add(new LineTuple(lt.c1, lt.c15()));
					lines.add(new LineTuple(lt.c15(), lt.c2));
				}
			}
		}
		
		return d;
	}
	
	private double lineSdf(Vector2D p, LineTuple lt) {
		Vector2D c1 = polyline[lt.c1];
		Vector2D c2 = polyline[lt.c2];
		
		Vector2D pa = p.subtract(c1);
		Vector2D ba = c2.subtract(c1);
		double h = pa.dotProduct(ba) / ba.dotProduct(ba);
		h = Math.min(Math.max(h, 0.0), 1.0);
		return pa.subtract(h, ba).getNorm();
	}
	
	private double lineLength(LineTuple lt) {
		Vector2D c1 = polyline[lt.c1];
		Vector2D c2 = polyline[lt.c2];
		
		return c1.distance(c2);
	}
	
	private class LineTuple {
		final int c1, c2; // Indexes
		
		public LineTuple(int c1, int c2) {
			this.c1 = c1;
			this.c2 = c2;
		}
		
		public boolean canSplit() {
			return c2 - c1 > 1;
		}
		
		public int c15() {
			return c1 + (c2 - c1) / 2;
		}
	}
}
