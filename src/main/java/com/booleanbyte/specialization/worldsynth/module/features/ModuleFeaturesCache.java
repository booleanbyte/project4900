package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;

public class ModuleFeaturesCache extends AbstractModule {
	
	private DatatypeFeatures cache = null;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		// Request input if there is no cached value
		if (cache == null) {
			inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		}
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		
		// Update cache value if there is input
		if (inputs.get("input") != null) {
			cache = (DatatypeFeatures) inputs.get("input");
		}
		
		// Return the cache
		return cache;
	}

	@Override
	public String getModuleName() {
		return "Features cache";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.UNKNOWN;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		Button clearCacheButton = new Button("Clear cache");
		clearCacheButton.setOnAction(e -> {
			cache = null;
		});
		
		pane.add(new Label("Clear cache"), 0, 0);
		pane.add(clearCacheButton, 1, 0);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {};
		
		return applyHandler;
	}
}
