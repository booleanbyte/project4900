package com.booleanbyte.specialization.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleHeightmapFeaturesToSdfHeightmap extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("features", new ModuleInputRequest(getInput("Features"), new DatatypeFeatures()));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Check if inputs are available
		if (inputs.get("features") == null) {
			//If there is not enough input just return null
			return null;
		}
				
		DatatypeFeatures featuresData = (DatatypeFeatures) inputs.get("features");
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float h = (float) getSdfAt(x+u*res, z+v*res, featuresData.getFeatures());
				map[u][v] = h;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
		
	public double getSdfAt(double x, double z, List<ShapedFeature> features) {
		Vector2D p = new Vector2D(x, z);
		
		double d = Double.POSITIVE_INFINITY;
		for (ShapedFeature s: features) {
			d = Math.min(d, s.sdf(p));
		}
		
		return d;
	}

	@Override
	public String getModuleName() {
		return "Features to SDF";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Features")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
