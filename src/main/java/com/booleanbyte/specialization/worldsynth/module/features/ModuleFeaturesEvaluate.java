package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.feature.Merger;
import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;
import com.github.davidmoten.rtree2.Entries;
import com.github.davidmoten.rtree2.Entry;
import com.github.davidmoten.rtree2.RTree;
import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Geometry;

import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.IntegerParameter;

public abstract class ModuleFeaturesEvaluate extends AbstractModule {
	
	protected final BooleanParameter selective = new BooleanParameter(
			"selective", "Selective",
			"Use efficient selective evaluation.",
			true);
	
	protected final IntegerParameter grouping = new IntegerParameter(
			"grouping", "Grouping",
			"Evaluation grouping size during evaluation. Value of 1 performs no grouping.",
			1, 1, Integer.MAX_VALUE, 1, 64);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				selective,
				grouping
		};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("features", new ModuleInputRequest(getInput("Features"), new DatatypeFeatures()));
		
		return inputRequests;
	}
	
	protected final RTree<ShapedFeature, Geometry> createRtree(List<ShapedFeature> features) {
		// Construct R-tree
		List<Entry<ShapedFeature, Geometry>> entries = new ArrayList<Entry<ShapedFeature, Geometry>>();
		for (ShapedFeature feature: features) {
			Entry<ShapedFeature, Geometry> e = Entries.entry(feature, feature.getBoundGeometry());
			entries.add(e);
		}
		RTree<ShapedFeature, Geometry> rtree = RTree.star().maxChildren(6).create(entries);
		
		return rtree;
	}
	
	protected final Merger evaluateAt(double x, double z, RTree<ShapedFeature, Geometry> features) {
		return evaluateAt(x, z, features.search(Geometries.point(x, z)));
	}
	
	protected final Merger evaluateAt(double x, double z, Iterable<Entry<ShapedFeature, Geometry>> features) {
		Merger merger = new Merger();
		Vector2D p = new Vector2D(x, z);
		
		for (Entry<ShapedFeature, Geometry> e: features) {
			merger.mergedFeatures++;
			e.value().merge(merger, p);
		}
		
		return merger;
	}
	
	protected final Merger evaluateAt(double x, double z, List<ShapedFeature> features) {
		Merger merger = new Merger();
		Vector2D p = new Vector2D(x, z);
		
		for (ShapedFeature e: features) {
			merger.mergedFeatures++;
			e.merge(merger, p);
		}
		
		return merger;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Features")
				};
		return i;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
