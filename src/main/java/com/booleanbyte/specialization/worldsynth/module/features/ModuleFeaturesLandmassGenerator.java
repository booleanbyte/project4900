package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.feature.Attribute;
import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.shape.Polygon;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeAttributes;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleFeaturesLandmassGenerator extends AbstractModule {

	private final DoubleParameter size = new DoubleParameter("size", "Size",
			"Aproximate radius of the landmass",
			1000, 0, Double.POSITIVE_INFINITY, 0, 10000);
	
	private final IntegerParameter initialVertices = new IntegerParameter("initialvertices", "Initial vertices",
			"The number of verticies in the initial step of the landmass shape",
			6, 3, Integer.MAX_VALUE, 3, 18);
	
	private final DoubleParameter sizeVariation = new DoubleParameter("sizevariation", "Size variation",
			"The max deviation of the initial shape from the size radius",
			100, 0, Double.POSITIVE_INFINITY, 0, 1000);
	
	private final IntegerParameter midpointDisplacements = new IntegerParameter("midpointdisplacements", "Midpoint displacements",
			"The number of midpoint displacement iteratoions to perform",
			2, 0, 10);
	
	private final DoubleParameter midpointDisplacementFactor = new DoubleParameter("midpointdisplacementfactor", "Midpoint displacement factor",
			"The relative max amount of midpoint displacement to perform",
			0.1, 0.0, 0.5);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				size,
				initialVertices,
				sizeVariation,
				midpointDisplacements,
				midpointDisplacementFactor,
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("attributes", new ModuleInputRequest(getInput("Attributes"), new DatatypeAttributes()));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//
		
		double size = this.size.getValue();
		int initialVertices = this.initialVertices.getValue();
		double sizeVariation = this.sizeVariation.getValue();
		int midpointDisplacements = this.midpointDisplacements.getValue();
		double midpointDisplacementFactor = this.midpointDisplacementFactor.getValue();
		
		// Read in the attributes input
		List<Attribute> attributes = new ArrayList<Attribute>();
		if (inputs.get("attributes") != null) {
			DatatypeAttributes attributesData = (DatatypeAttributes) inputs.get("attributes");
			attributes = attributesData.getAttributes();
		}
		
		//----------BUILD----------//
		
		Random r = new Random(0);
		
		LinkedList<Vector2D> vertexes = new LinkedList<>();
		
		int vs = initialVertices;
		for (int i = 0; i < vs; i++) {
			double mod = (r.nextDouble() - 0.5) * sizeVariation;
			Vector2D v = new Vector2D((size + mod) * Math.cos(i * 2 * Math.PI / (double) vs), (size  + mod) * Math.sin(i * 2 * Math.PI / (double) vs));
			vertexes.add(v);
		}
		
		// Midpoint displacement
		for (int i = 1; i <= midpointDisplacements; i++) {
			int j = 0;
			while (j < vertexes.size()) {
				Vector2D v1 = vertexes.get(j);
				Vector2D v2 = vertexes.get((j+1)%vertexes.size());
				Vector2D dir = v2.subtract(v1);
				Vector2D norm = new Vector2D(-dir.getY(), dir.getX()).normalize();
				
				double displacement = (r.nextDouble()*2.0-1.0)*dir.getNorm()*midpointDisplacementFactor;
				Vector2D v15 = v1.add(dir.scalarMultiply(0.5)).add(norm.scalarMultiply(displacement));
				
				vertexes.add(++j, v15);
				j++;
			}
		}
		
		Polygon landmassShape = new Polygon(0.0, true, vertexes);
		
		ArrayList<ShapedFeature> features = new ArrayList<ShapedFeature>();
		
		ShapedFeature landmassFeature = new ShapedFeature(landmassShape, attributes);
		landmassFeature.setAnnotationColor(Color.RED);
		features.add(landmassFeature);
		
		requestData.setFeatures(features);
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Landmass generator";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeAttributes(), "Attributes")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

}
