package com.booleanbyte.specialization.feature;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.shape.Shape;
import com.github.davidmoten.rtree2.geometry.Geometry;

import javafx.scene.paint.Color;

public class ShapedFeature {
	
	public static final Function<Double, Double> DEFAULT_SDF_MODIFIER = d -> d;
	
	private Color annotationColor = Color.WHITE;
	
	private final Shape shape;
	private final List<Attribute> attributes;
	final Function<Double, Double> sdfModifier;
	
	public ShapedFeature(Shape shape) {
		this(shape, new ArrayList<Attribute>());
	}
	
	public ShapedFeature(Shape shape, List<Attribute> attributes) {
		this(shape, attributes, DEFAULT_SDF_MODIFIER);
	}
	
	public ShapedFeature(Shape shape, List<Attribute> attributes, Function<Double, Double> sdfModifier) {
		this.shape = shape;
		this.attributes = attributes;
		this.sdfModifier = sdfModifier;
	}
	
	public void setAnnotationColor(Color annotationColor) {
		this.annotationColor = annotationColor;
	}
	
	public Color getAnnotationColor() {
		return annotationColor;
	}
	
	public Shape getShape() {
		return shape;
	}
	
	public void addAttribute(Attribute attribute) {
		attributes.add(attribute);
	}
	
	public List<Attribute> getAttributes() {
		return attributes;
	}
	
	public Function<Double, Double> getSdfModifier() {
		return sdfModifier;
	}
	
	public double sdf(Vector2D p) {
		return sdfModifier.apply(shape.sdf(p));
	}
	
	public double height(Vector2D p) {
		return shape.height(p);
	}
	
	/**
	 * Merge the feature into results for evaluated point p.
	 * 
	 * @param merger the results merger.
	 * @param p the evaluated point.
	 */
	public void merge(Merger merger, Vector2D p) {
		double sdf = sdf(p);
		
		for (Attribute attr: attributes) {
			attr.contrubute(merger, this, p, sdf);
		}
	}
	
	/**
	 * Get the bounding geometry covering the feature's area of influence.
	 * 
	 * @return the influence bounding geometry.
	 */
	public Geometry getBoundGeometry() {
		double offset = 0;

		for (Attribute attr: attributes) {
			offset = Math.max(offset, attr.influenceDistance());
		}
		
		return shape.getBoundGeometry(offset);
	}
}
