package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.feature.Attribute;
import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.shape.Line;
import com.booleanbyte.specialization.shape.Shape;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeAttributes;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleFeaturesRiverNetworkGenerator extends AbstractModule {

	private final DoubleParameter segementLenght = new DoubleParameter("minimumsegmentlenght", "Minimum segment length",
			"Minimum lenght of a single river segment",
			50, 1, Double.POSITIVE_INFINITY, 1, 200);
	
	private final DoubleParameter segmentLenghtFactor = new DoubleParameter("maximumsegmentlenghtfactor", "Maximum segment length factor",
			"Multiplication factor for random additional max segement length",
			0.5, 0.0, 1.0);
	
	private final DoubleParameter slope = new DoubleParameter("slope", "Slope",
			"How steep the river rises away from the shore",
			0.1, 0.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				segementLenght,
				segmentLenghtFactor,
				slope
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("landmass", new ModuleInputRequest(getInput("Landmass"), new DatatypeFeatures()));
		
		inputRequests.put("attributes", new ModuleInputRequest(getInput("Attributes"), new DatatypeAttributes()));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//
		
		double segementLenght = this.segementLenght.getValue();
		double segmentLenghtFactor = this.segmentLenghtFactor.getValue();
		double slopeAngle = Math.atan(this.slope.getValue());
		
		//Check if inputs are available
		if (inputs.get("landmass") == null) {
			//If there is not enough input just return null
			return null;
		}
		
		// Read in the landmass input
		DatatypeFeatures landmassData = (DatatypeFeatures) inputs.get("landmass");
		List<ShapedFeature> landmassFeatures = landmassData.getFeatures();
		
		// Read in the attributes input
		List<Attribute> attributes = new ArrayList<Attribute>();
		if (inputs.get("attributes") != null) {
			DatatypeAttributes attributesData = (DatatypeAttributes) inputs.get("attributes");
			attributes = attributesData.getAttributes();
		}
		
		//----------BUILD----------//
		
		// Get all verticies
		HashSet<Vector3D> landmasVerticies = new HashSet<Vector3D>();
		for (ShapedFeature feature : landmassFeatures) {
			feature.getShape().collect3dVerticies(landmasVerticies);
		}
		
		// Make seeding nodes
		ArrayList<RiverNode> riverNodes = new ArrayList<>();
		LinkedList<RiverNode> seedingNodes = new LinkedList<>();
		for (Vector3D v : landmasVerticies) {
			if (validatePlacement(v, riverNodes, segementLenght, null)) {
				RiverNode n = new RiverNode(v, null);
				riverNodes.add(n);
				seedingNodes.add(n);
			}
		}
		
		Random r = new Random(0);
		
		int maxSamples = 30;
		while (seedingNodes.size() > 0) {
			RiverNode seedingNode = seedingNodes.remove(r.nextInt(seedingNodes.size()));
			
			for (int i = 0; i < maxSamples; i++) {
				// Decide a random length and direction towards where to try a new node at
				double rdist = segementLenght + segementLenght * segmentLenghtFactor * r.nextDouble();
				double rdir = Math.PI * (r.nextDouble() * 2.0 - 1.0);
				
				// Create a new vertex vector
				Vector3D v = new Vector3D(rdir, slopeAngle).scalarMultiply(rdist).add(seedingNode.vert);
				
				// Check if the new vertex is a valid placement for a new river node
				if (validatePlacement(v, riverNodes, segementLenght, landmassFeatures)) {
					RiverNode n = new RiverNode(v, seedingNode);
					riverNodes.add(n);
					seedingNodes.add(n);
					seedingNodes.add(seedingNode);
					break;
				}
			}
		}
		
		// Features
		ArrayList<ShapedFeature> riverFeatures = new ArrayList<ShapedFeature>();
		
		for (Shape s: createRiverNetworkShapes(riverNodes)) {
			ShapedFeature riverFeature = new ShapedFeature(s, attributes);
			riverFeature.setAnnotationColor(Color.CADETBLUE);
			riverFeatures.add(riverFeature);
		}
		
		requestData.setFeatures(riverFeatures);
		return requestData;
	}
	
	private boolean validatePlacement(Vector3D v, List<RiverNode> nodes, double minDist, List<ShapedFeature> landmassFeatures) {
		Vector2D v2d = new Vector2D(v.getX(), v.getY());
		
		double d = Double.POSITIVE_INFINITY;
		for (RiverNode n: nodes) {
			Vector2D vn = new Vector2D(n.vert.getX(), n.vert.getY());
			d = Math.min(d, vn.distance(v2d));
		}
		
		if(landmassFeatures != null) {
			for (ShapedFeature landmass : landmassFeatures) {
				if (landmass.sdf(v2d) < minDist) return false;
			}
		}
		
		return d > minDist;
	}
	
	public List<Shape> createRiverNetworkShapes(List<RiverNode> riverNodes) {
		ArrayList<Shape> riverSegments = new ArrayList<>();
		
		for (RiverNode node : riverNodes) {
			if (node.parent == null) continue;
			
			Line riverSegment = new Line(
					node.parent.vert.getX(),
					node.parent.vert.getY(),
					node.parent.vert.getZ(),
					node.vert.getX(),
					node.vert.getY(),
					node.vert.getZ());
			riverSegments.add(riverSegment);
		}
		
		return riverSegments;
	}

	@Override
	public String getModuleName() {
		return "River network generator";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Landmass"),
				new ModuleInput(new DatatypeAttributes(), "Attributes")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private class RiverNode {
		public final Vector3D vert;
		public final RiverNode parent;
		
		public RiverNode(Vector3D vert, RiverNode parent) {
			this.vert = vert;
			this.parent = parent;
		}
	}
}
