package com.booleanbyte.specialization.shape;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Rectangle;

public class FractalPolygon extends Polygon {
	
	protected final double mpdFactor;
	protected final int mpdIterations;
	
	private static List<Vector2D> mpdPolygon(List<Vector2D> polygon, double displacementFactor, int iterations) {
		Random r = new Random(polygon.hashCode());
		
		List<Vector2D> vertices = new LinkedList<Vector2D>(polygon);
		
		// Midpoint displacement
		for (int i = 1; i <= iterations; i++) {
			int j = 0;
			while (j < vertices.size()) {
				Vector2D v1 = vertices.get(j);
				Vector2D v2 = vertices.get((j+1)%vertices.size());
				Vector2D dir = v2.subtract(v1);
				Vector2D norm = new Vector2D(-dir.getY(), dir.getX()).normalize();
				
				double displacement = (r.nextDouble()*2.0-1.0)*dir.getNorm()*displacementFactor;
				Vector2D v15 = v1.add(dir.scalarMultiply(0.5)).add(norm.scalarMultiply(displacement));
				
				vertices.add(++j, v15);
				j++;
			}
		}
		
		return vertices;
	}
	
	public FractalPolygon(double height, boolean inverted, List<Vector2D> vertices, double mpdFactor, int mpdIterations) {
		super(height, inverted, mpdPolygon(vertices , mpdFactor, mpdIterations));
		
		this.mpdFactor = mpdFactor;
		this.mpdIterations = mpdIterations;
	}
	
	@Override
	public Rectangle getBoundGeometry(double offset) {
		double x1 = Double.POSITIVE_INFINITY;
		double y1 = Double.POSITIVE_INFINITY;
		double x2 = Double.NEGATIVE_INFINITY;
		double y2 = Double.NEGATIVE_INFINITY;
		
		for (Vector2D v: polygon) {
			x1 = Math.min(x1, v.getX());
			y1 = Math.min(y1, v.getY());
			x2 = Math.max(x2, v.getX());
			y2 = Math.max(y2, v.getY());
		}
		
		return Geometries.rectangle(x1 - offset, y1 - offset, x2 + offset, y2 + offset);
	}
}
