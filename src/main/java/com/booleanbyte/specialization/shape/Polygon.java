package com.booleanbyte.specialization.shape;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Rectangle;

import javafx.scene.canvas.GraphicsContext;

public class Polygon implements Shape, ConnectingPointsShape {
	
	protected final Vector2D[] polygon;
	protected final double h;
	protected final boolean inverted;
	
	public Polygon(double height, boolean inverted, List<Vector2D> vertices) {
		this.polygon = vertices.toArray(new Vector2D[0]);
		this.h = height;
		this.inverted = inverted;
	}
	
	public double getHeight() {
		return h;
	}
	
	public boolean isInverted() {
		return inverted;
	}
	
	@Override
	public boolean areConnected(Vector2D p1, Vector2D p2) {
		int ip2;
		for (ip2 = 0; ip2 < polygon.length; ip2++) {
			if (polygon[ip2].equals(p2)) break;
		}
		if (ip2 >= polygon.length) return false;
		
		int ip1 = ip2-1 >= 0 ? ip2-1 : polygon.length-1;
		int ip3 = ip2+1 < polygon.length ? ip2+1 : 0;
		
		// Check if p1 is directly before or after p2
		return polygon[ip1].equals(p1) || polygon[ip3].equals(p1);
	}
	
	@Override
	public double sdf(Vector2D p) {
		double d = p.subtract(polygon[0]).dotProduct(p.subtract(polygon[0]));
		double s = 1.0;
		for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i, i++) {
			Vector2D e = polygon[j].subtract(polygon[i]);
			Vector2D w = p.subtract(polygon[i]);
			double dot_we = w.dotProduct(e);
			double dot_ee = e.dotProduct(e);
			Vector2D b = w.subtract(e.scalarMultiply(clamp(dot_we / dot_ee, 0.0, 1.0)));
			d = Math.min(d, b.dotProduct(b));

			boolean[] c = {
					p.getY() >= polygon[i].getY(),
					p.getY() < polygon[j].getY(),
					e.getX() * w.getY() > e.getY() * w.getX()
					};
			
			if (all(c) || allNot(c)) s *= -1.0;
		}
		
		if (inverted) {
			return -s * Math.sqrt(d);
		}
		return s * Math.sqrt(d);
	}
	
	protected final double clamp(double a, double min, double max) {
		return Math.min(Math.max(a, min), max);
	}
	
	protected final boolean all(boolean[] bs) {
		for (boolean b: bs) {
			if (!b) return false;
		}
		return true;
	}
	
	protected final boolean allNot(boolean[] bs) {
		for (boolean b: bs) {
			if (b) return false;
		}
		return true;
	}
	
	@Override
	public double height(Vector2D p) {
		return h;
	}
	
	@Override
	public void paint(GraphicsContext g) {
		for (int i = 1; i < polygon.length; i++) {
			g.strokeLine(polygon[i-1].getX(), polygon[i-1].getY(), polygon[i].getX(), polygon[i].getY());
		}
		g.strokeLine(polygon[polygon.length-1].getX(), polygon[polygon.length-1].getY(), polygon[0].getX(), polygon[0].getY());
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		for (Vector2D p: polygon) {
			pointsCollection.put(p, h);
		}
	}

	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		for (Vector2D v : polygon) {
			verticies.add(v);
		}
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		for (Vector2D v : polygon) {
			verticies.add(new Vector3D(v.getX(), v.getY(), h));
		}
	}
	
	@Override
	public Rectangle getBoundGeometry(double offset) {
		double x1 = Double.POSITIVE_INFINITY;
		double y1 = Double.POSITIVE_INFINITY;
		double x2 = Double.NEGATIVE_INFINITY;
		double y2 = Double.NEGATIVE_INFINITY;
		
		for (Vector2D v: polygon) {
			x1 = Math.min(x1, v.getX());
			y1 = Math.min(y1, v.getY());
			x2 = Math.max(x2, v.getX());
			y2 = Math.max(y2, v.getY());
		}
		
		return Geometries.rectangle(x1 - offset, y1 - offset, x2 + offset, y2 + offset);
	}
}
