package com.booleanbyte.specialization.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization.feature.Attribute;
import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.shape.DynamicFractalPolygon;
import com.booleanbyte.specialization.shape.DynamicFractalPolyline;
import com.booleanbyte.specialization.shape.FractalPolygon;
import com.booleanbyte.specialization.shape.FractalPolyline;
import com.booleanbyte.specialization.shape.Line;
import com.booleanbyte.specialization.shape.Polygon;
import com.booleanbyte.specialization.shape.Shape;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeAttributes;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleFeaturesMidpointDisplacement extends AbstractModule {

	private final IntegerParameter midpointDisplacements = new IntegerParameter("midpointdisplacements", "Midpoint displacements",
			"The number of midpoint displacement iteratoions to perform",
			2, 0, 10);
	
	private final DoubleParameter midpointDisplacementFactor = new DoubleParameter("midpointdisplacementfactor", "Midpoint displacement factor",
			"The relative max amount of midpoint displacement to perform",
			0.1, 0.0, 0.5);
	
	private final EnumParameter<LineMethod> lineMethod = new EnumParameter<LineMethod>("linemethod", "Line method",
			"The applicatiom method for line shapes",
			LineMethod.class, LineMethod.STATIC);
	
	private final EnumParameter<PolygonMethod> polygonMethod = new EnumParameter<PolygonMethod>("polygonmethod", "Polygon method",
			"The application method for polygon shapes",
			PolygonMethod.class, PolygonMethod.STATIC);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				midpointDisplacements,
				midpointDisplacementFactor,
				lineMethod,
				polygonMethod
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), new DatatypeFeatures()));
		
		inputRequests.put("attributes", new ModuleInputRequest(getInput("Attributes"), new DatatypeAttributes()));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//

		int midpointDisplacements = this.midpointDisplacements.getValue();
		double midpointDisplacementFactor = this.midpointDisplacementFactor.getValue();
		LineMethod lineMethod = this.lineMethod.getValue();
		PolygonMethod polygonMethod = this.polygonMethod.getValue();
		
		// Check if both inputs are available
		if (inputs.get("input") == null) {
			//If the input is null, there is not enough input and then just return null
			return null;
		}
		
		// Read in the features input
		List<ShapedFeature> inputfeatures = ((DatatypeFeatures) inputs.get("input")).getFeatures();
		
		// Read in the attributes input
		List<Attribute> attributes = null;
		if (inputs.get("attributes") != null) {
			DatatypeAttributes attributesData = (DatatypeAttributes) inputs.get("attributes");
			attributes = attributesData.getAttributes();
		}
		
		//----------BUILD----------//
		
		// Features
		ArrayList<ShapedFeature> mpdFeatures = new ArrayList<ShapedFeature>();
		
		for (ShapedFeature feature: inputfeatures) {
			if (feature.getShape() instanceof Line) {
				Line line = (Line) feature.getShape();
				
				// Midpoint displacement
				List<Shape> mpdLines = mpdLine(line, midpointDisplacements, midpointDisplacementFactor, lineMethod);
				
				// Make line features
				for (Shape l: mpdLines) {
					ShapedFeature lineFeature;
					if (attributes != null) {
						lineFeature = new ShapedFeature(l, attributes, feature.getSdfModifier());
					}
					else {
						lineFeature = new ShapedFeature(l, feature.getAttributes(), feature.getSdfModifier());
					}
					lineFeature.setAnnotationColor(feature.getAnnotationColor());
					mpdFeatures.add(lineFeature);
				}
			}
			else if (feature.getShape() instanceof Polygon) {
				Polygon polygon = (Polygon) feature.getShape();
				
				// Midpoint displacement
				Shape mpdPolygon = mpdPolygon(polygon, midpointDisplacements, midpointDisplacementFactor, polygonMethod);
				
				// Make polygon feature
				if (mpdPolygon != null) {
					ShapedFeature polygonFeature;
					if (attributes != null) {
						polygonFeature = new ShapedFeature(mpdPolygon, attributes, feature.getSdfModifier());
					}
					else {
						polygonFeature = new ShapedFeature(mpdPolygon, feature.getAttributes(), feature.getSdfModifier());
					}
					polygonFeature.setAnnotationColor(feature.getAnnotationColor());
					mpdFeatures.add(polygonFeature);
				}
			}
		}
		
		requestData.setFeatures(mpdFeatures);
		return requestData;
	}
	
	private List<Shape> mpdLine(Line line, int iterations, double displacementFactor, LineMethod method) {
		ArrayList<Shape> mpdLines = new ArrayList<Shape>();
		
		List<Vector3D> v = new LinkedList<Vector3D>();
		line.collect3dVerticies(v);
		
		switch (method) {
		case NONE:
			mpdLines.add(line);
			break;
			
		case SPLIT:
			Vector2D c1 = new Vector2D(v.get(0).getX(), v.get(0).getY());
			Vector2D c2 = new Vector2D(v.get(1).getX(), v.get(1).getY());
			
			// Midpoint displacement
			Random r = new Random(c1.hashCode() ^ c2.hashCode());
			for (int i = 1; i <= iterations; i++) {
				int j = 0;
				while (j < v.size()-1) {
					Vector3D v1 = v.get(j);
					Vector3D v2 = v.get((j+1)%v.size());
					Vector3D dir = v2.subtract(v1);
					Vector3D norm = new Vector3D(-dir.getY(), dir.getX(), 0.0).normalize();
					
					double displacement = (r.nextDouble()*2.0-1.0)*dir.getNorm()*displacementFactor;
					Vector3D v15 = v1.add(dir.scalarMultiply(0.5)).add(norm.scalarMultiply(displacement));
					
					v.add(++j, v15);
					j++;
				}
			}
			
			// Make lines
			for (int i = 1; i < v.size(); i++) {
				mpdLines.add(new Line(v.get(i-1), v.get(i)));
			}
			break;
			
		case STATIC:
			mpdLines.add(new FractalPolyline(v.get(0), v.get(1), displacementFactor, iterations));
			break;
			
		case DYNAMIC:
			mpdLines.add(new DynamicFractalPolyline(v.get(0), v.get(1), displacementFactor, iterations));
			break;
			
		default:
			// Line to nothing
			break;
		}
		
		return mpdLines;
	}
	
	private Shape mpdPolygon(Polygon polygon, int iterations, double displacementFactor, PolygonMethod method) {
		List<Vector2D> v = new LinkedList<Vector2D>();
		polygon.collect2dVerticies(v);
		
		switch (method) {
		case NONE:
			return polygon;
			
		case STATIC:
			return new FractalPolygon(polygon.getHeight(), polygon.isInverted(), v, displacementFactor, iterations);
			
		case DYNAMIC:
			return new DynamicFractalPolygon(polygon.getHeight(), polygon.isInverted(), v, displacementFactor, iterations);
			
		default:
			// Polygon to nothing
			return null;
		}
	}

	@Override
	public String getModuleName() {
		return "MPD";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Input"),
				new ModuleInput(new DatatypeAttributes(), "Attributes")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum LineMethod {
		NONE,
		SPLIT,
		STATIC,
		DYNAMIC,
		REMOVE;
	}
	
	private enum PolygonMethod {
		NONE,
		STATIC,
		DYNAMIC,
		REMOVE;
	}
}
