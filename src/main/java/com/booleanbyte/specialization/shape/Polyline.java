package com.booleanbyte.specialization.shape;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Rectangle;

import javafx.scene.canvas.GraphicsContext;

public class Polyline implements Shape, ConnectingPointsShape {
	
	final Vector2D c1, c2;
	final Double h1, h2;
	
	final protected Vector2D[] polyline;
	
	public Polyline(List<Vector2D> polyline, double height1, double height2) {
		this.c1 = polyline.get(0);
		this.c2 = polyline.get(polyline.size()-1);
		this.h1 = height1;
		this.h2 = height2;
		
		this.polyline = polyline.toArray(new Vector2D[0]);
	}
		
	@Override
	public boolean areConnected(Vector2D p1, Vector2D p2) {
		return (c1.equals(p1) && c2.equals(p2)) || (c1.equals(p2) && c2.equals(p1));
	}
	
	@Override
	public double sdf(Vector2D p) {
		double d = Double.POSITIVE_INFINITY;
		for (int i = 1; i < polyline.length; i++) {
			double d_ = lineSdf(p, polyline[i-1], polyline[i]);
			d = Math.min(d_, d);
		}
		return d;
	}
	
	private double lineSdf(Vector2D p, Vector2D c1, Vector2D c2) {
		Vector2D pa = p.subtract(c1);
		Vector2D ba = c2.subtract(c1);
		double h = pa.dotProduct(ba) / ba.dotProduct(ba);
		h = Math.min(Math.max(h, 0.0), 1.0);
		return pa.subtract(h, ba).getNorm();
	}
	
	@Override
	public double height(Vector2D p) {
		Vector2D pa = p.subtract(c1);
		Vector2D ba = c2.subtract(c1);
		double h = pa.dotProduct(ba) / ba.dotProduct(ba);
		h = Math.min(Math.max(h, 0.0), 1.0);
		return lerp(h1, h2, h);
	}
	
	/**
	 * Linear interpolation from a to b where t is in the range [0, 1].
	 * 
	 * <p>
	 * {@code a + t * (b - a)}
	 * 
	 * @param a
	 * @param b
	 * @param t
	 * @return
	 */
	private final double lerp(double a, double b, double t) {
		return a + t * (b - a);
	}

	@Override
	public void paint(GraphicsContext g) {
		for (int i = 1; i < polyline.length; i++) {
			g.strokeLine(polyline[i-1].getX(), polyline[i-1].getY(), polyline[i].getX(), polyline[i].getY());
		}
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		pointsCollection.put(c1, h1);
		pointsCollection.put(c2, h2);
	}
	
	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		verticies.add(c1);
		verticies.add(c2);
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		verticies.add(new Vector3D(c1.getX(), c1.getY(), h1));
		verticies.add(new Vector3D(c2.getX(), c2.getY(), h2));
	}

	@Override
	public Rectangle getBoundGeometry(double offset) {
		double x1 = Double.POSITIVE_INFINITY;
		double y1 = Double.POSITIVE_INFINITY;
		double x2 = Double.NEGATIVE_INFINITY;
		double y2 = Double.NEGATIVE_INFINITY;
		
		for (Vector2D v: polyline) {
			x1 = Math.min(x1, v.getX());
			y1 = Math.min(y1, v.getY());
			x2 = Math.max(x2, v.getX());
			y2 = Math.max(y2, v.getY());
		}
		
		return Geometries.rectangle(
				x1 - offset,
				y1 - offset,
				x2 + offset,
				y2 + offset);
	}
}
