package com.booleanbyte.specialization.feature;

import java.util.ArrayList;

public class ElevationPriorityMerge {
	
	ArrayList<ElevationMerge> elevations = new ArrayList<ElevationMerge>();
	
	public void contributeElevation(double elevation, double weight, int priority) {
		while (priority > elevations.size()-1) {
			elevations.add(new ElevationMerge());
		}
		
		elevations.get(priority).contributeElevation(elevation, weight);
	}
	
	public double getElevation() {
		double elevation = elevations.get(0).getElevation();
		
		for (int i = 1; i < elevations.size(); i++) {
			ElevationMerge pe = elevations.get(i);
			double w = pe.getWeightSum();
			if (w <= 0.0) continue;
			
			double e = pe.getElevation();
			elevation = lerp(elevation, e, Math.min(w, 1.0));
		}
		
		return elevation;
	}
	
	private double lerp(double a, double b, double t) {
		return a + (b - a) * t;
	}
}
