package com.booleanbyte.specialization.feature;

import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class WaterDepthAttribute implements Attribute {
	
	private final Function<Double, Double> depthFunction;
	private final WeightFunction weightFunction;
	
	public WaterDepthAttribute(Function<Double, Double> depthFunction, WeightFunction weightFunction) {
		this.depthFunction = depthFunction;
		this.weightFunction = weightFunction;
	}
	
	@Override
	public void contrubute(Merger merger, ShapedFeature feature, Vector2D p, double sdf) {
		double weight = weightFunction.apply(sdf);
		
		double depth = depthFunction.apply(sdf);
		
		merger.contributeWaterDepth(depth, weight);
	}
	
	@Override
	public double influenceDistance() {
		return weightFunction.influenceDistance();
	}
}
