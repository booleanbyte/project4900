package com.booleanbyte.specialization.feature;

import java.util.IdentityHashMap;
import java.util.Map.Entry;

import net.worldsynth.material.MaterialState;

public class Merger {
	
	public int mergedFeatures = 0;
	
	// Surface elevation
	private final ElevationPriorityMerge elevation = new ElevationPriorityMerge();
	
	public void contributeElevation(double elevation, double weight, int priority) {
		this.elevation.contributeElevation(elevation, weight, priority);
	}
	
	public double getElevation() {
		return elevation.getElevation();
	}
	
	// Surface material
	private IdentityHashMap<MaterialState<?, ?>, Double> surfaceMaterialMix = new IdentityHashMap<MaterialState<?,?>, Double>();
	
	public void contributeMaterial(MaterialState<?, ?> material, double weight) {
		Double currentWeight = surfaceMaterialMix.get(material);
		if (currentWeight == null) {
			surfaceMaterialMix.put(material, weight);
		}
		else {
			surfaceMaterialMix.put(material, currentWeight + weight);
		}
	}
	
	public MaterialState<?, ?> getMaterial() {
		double weight = 0.0;
		MaterialState<?, ?> material = null;
		
		for (Entry<MaterialState<?, ?>, Double> entry: surfaceMaterialMix.entrySet()) {
			if (entry.getValue() > weight) {
				weight = entry.getValue();
				material = entry.getKey();
			}
		}
		
		return material;
	}
	
	// Surface water depth
	private double waterDepthWeightSum = 0.0;
	private double waterDepthSum = 0.0;
	
	public void contributeWaterDepth(double depth, double weight) {
		waterDepthWeightSum += weight;
		waterDepthSum += depth * weight;
	}
	
	public double getWaterDepth() {
		return waterDepthSum / waterDepthWeightSum;
	}
}
