package com.booleanbyte.specialization.worldsynth.datatype;

import java.util.ArrayList;
import java.util.List;

import com.booleanbyte.specialization.feature.Attribute;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class DatatypeAttributes extends AbstractDatatype {
	
	private List<Attribute> attributes;
	
	public List<Attribute> getAttributes() {
		return attributes;
	}
	
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.PURPLE;
	}

	@Override
	public String getDatatypeName() {
		return "Attributes";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeAttributes df = new DatatypeAttributes();
		if (attributes != null) df.attributes = new ArrayList<Attribute>(attributes);
		return df;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeAttributes();
	}
	
	@Override
	public AbstractPreviewRender getPreviewRender() {
		return null;
	}
}
