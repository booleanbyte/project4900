package com.booleanbyte.specialization.shape;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class DynamicFractalPolygon extends FractalPolygon {
	
	public DynamicFractalPolygon(double height, boolean inverted, List<Vector2D> vertices, double mpdFactor, int mpdIterations) {
		super(height, inverted, vertices, mpdFactor, mpdIterations);
	}
	
	@Override
	public double sdf(Vector2D p) {
		double d = Double.POSITIVE_INFINITY; // Result distance
		double s = 1.0;
		
		LinkedList<LineTuple> lines = new LinkedList<LineTuple>();
		LinkedList<Double> dists = new LinkedList<Double>();
		LinkedList<Double> signs = new LinkedList<Double>();
		
		// Start with initial polygon sides
		int a = 1 << mpdIterations;
		for (int i = 0; i < polygon.length; i += a) {
			lines.add(new LineTuple(i, i+a));
		}
		
		
		while (lines.size() > 0) {
			double d_ = Double.POSITIVE_INFINITY;
			int size = lines.size();
			
			for (int j = 0; j < size; j++) {
				LineTuple lt = lines.get(j);
				double dd = lineSdf(p, lt);
				signs.add(Math.signum(dd));
				dd = Math.abs(dd);
				dists.add(dd);
				d_ = Math.min(d_, dd);
			}
			
			for(int j = 0; j < size; j++) {
				LineTuple lt = lines.remove();
				double dd = dists.remove();
				double ss = signs.remove();
				
				if (!lt.canSplit()) {
					// If the line segment is at max depth, apply for minimum distance of final result.
					d = Math.min(d, dd);
					s *= ss;
					continue;
				}
				
				double ll = lineLength(lt); // Line length
				double maxPDisp = ll * mpdFactor; // Max estimated distance change after displacement
				double minPDist = dd - maxPDisp; // Minimum estimated possible distance after displacement
				
				if (minPDist <= Math.min(d, d_)) {
					// If the distance may become closer than the current closest, split the line.
					lines.add(new LineTuple(lt.c1, lt.c15()));
					lines.add(new LineTuple(lt.c15(), lt.c2));
				}
				else {
					s *= ss;
				}
			}
		}
		
		return d * s;
	}
	
	private double lineSdf(Vector2D p, LineTuple lt) {
		Vector2D c1 = polygon[lt.c1];
		Vector2D c2 = polygon[lt.c2 % polygon.length];
		
		Vector2D e = c2.subtract(c1);
		Vector2D w = p.subtract(c1);
		double dot_we = w.dotProduct(e);
		double dot_ee = e.dotProduct(e);
		Vector2D b = w.subtract(e.scalarMultiply(clamp(dot_we / dot_ee, 0.0, 1.0)));
		double d = b.dotProduct(b);
		
		boolean[] c = {
				p.getY() >= c1.getY(),
				p.getY() < c2.getY(),
				e.getX() * w.getY() > e.getY() * w.getX()
				};
		double s = (all(c) || allNot(c)) ? -1.0 : 1.0;
		
		return Math.sqrt(d) * s;
	}
	
	private double lineLength(LineTuple lt) {
		Vector2D c1 = polygon[lt.c1];
		Vector2D c2 = polygon[lt.c2 % polygon.length];
		
		return c1.distance(c2);
	}
	
	private class LineTuple {
		final int c1, c2; // Indexes
		
		public LineTuple(int c1, int c2) {
			this.c1 = c1;
			this.c2 = c2;
		}
		
		public boolean canSplit() {
			return c2 - c1 > 1;
		}
		
		public int c15() {
			return c1 + (c2 - c1) / 2;
		}
	}
}
