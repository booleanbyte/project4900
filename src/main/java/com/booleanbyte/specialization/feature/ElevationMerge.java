package com.booleanbyte.specialization.feature;

public class ElevationMerge {
	
	private double elevationWeightSum = 0.0;
	private double elevationSum = 0.0;
	
	public void contributeElevation(double elevation, double weight) {
		elevationWeightSum += weight;
		elevationSum += elevation * weight;
	}
	
	public double getElevation() {
		return elevationSum / elevationWeightSum;
	}
	
	double getElevationSum() {
		return elevationSum;
	}
	
	double getWeightSum() {
		return elevationWeightSum;
	}
}
