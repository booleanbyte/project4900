package com.booleanbyte.specialization.worldsynth.patcher.ui.preview;

import java.util.List;

import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class FeaturesSimplifiedRender extends AbstractPreviewRenderCanvas {
	
	private List<ShapedFeature> features;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeFeatures castData = (DatatypeFeatures) data;
		this.features = castData.getFeatures();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.0));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.save();
		g.translate(getWidth()/2, getHeight()/2);
		g.scale(0.1, 0.1);
		g.setLineWidth(10);
		for (ShapedFeature feature: features) {
			g.setStroke(feature.getAnnotationColor());
			feature.getShape().paint(g);
		}
		g.restore();
	}
}
