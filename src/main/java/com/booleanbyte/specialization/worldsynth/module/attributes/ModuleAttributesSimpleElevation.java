package com.booleanbyte.specialization.worldsynth.module.attributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.specialization.feature.Attribute;
import com.booleanbyte.specialization.feature.ElevationAttribute;
import com.booleanbyte.specialization.feature.WeightFunction;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeAttributes;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.IntegerParameter;
import net.worldsynth.parameter.ObjectParameter;

public class ModuleAttributesSimpleElevation extends AbstractModule {
	
	private final ObjectParameter weightFunction = new ObjectParameter(
			"weightfunction", "Weight function",
			"The distance weight function to use evaluation the weigted average",
			WeightFunction.W3,
			WeightFunction.W1, WeightFunction.W2, WeightFunction.W3);
	
	private final DoubleParameter a = new DoubleParameter(
			"a", "a",
			"The distance-weight range",
			200, 0, Double.POSITIVE_INFINITY, 0, 200);
	
	private final DoubleParameter b = new DoubleParameter(
			"b", "b",
			"The distance-weight smoothing for W3",
			8, 0, Double.POSITIVE_INFINITY, 0, 10);
	
	private final IntegerParameter priority = new IntegerParameter(
			"priority", "Priority",
			"The elevation priority, higher value is higher priority.",
			0, 0, Integer.MAX_VALUE, 0, 4);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				weightFunction,
				a,
				b,
				priority
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeAttributes requestData = (DatatypeAttributes) request.data;
		
		//----------READ INPUTS----------//

		WeightFunction weightFunction = (WeightFunction) this.weightFunction.getValue();
		double a = this.a.getValue();
		double b = this.b.getValue();
		
		if (weightFunction == WeightFunction.W1) {
			weightFunction = WeightFunction.w1(a);
		}
		else if (weightFunction == WeightFunction.W2) {
			weightFunction = WeightFunction.w2(a);
		}
		else if (weightFunction == WeightFunction.W3) {
			weightFunction = WeightFunction.w3(a, b);
		}
		
		int priority = this.priority.getValue();
		
		//----------BUILD----------//
		
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		
		ElevationAttribute ea = new ElevationAttribute(weightFunction, priority);
		attributes.add(ea);
		
		requestData.setAttributes(attributes);
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Simple elevation attribute";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeAttributes(), "output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

}
