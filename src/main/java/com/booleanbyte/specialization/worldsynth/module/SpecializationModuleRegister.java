package com.booleanbyte.specialization.worldsynth.module;

import com.booleanbyte.specialization.worldsynth.module.attributes.ModuleAttributesCollect;
import com.booleanbyte.specialization.worldsynth.module.attributes.ModuleAttributesSimpleElevation;
import com.booleanbyte.specialization.worldsynth.module.attributes.ModuleAttributesSimpleMaterial;
import com.booleanbyte.specialization.worldsynth.module.attributes.ModuleAttributesSimpleWaterDepth;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeatureConflictRemoval;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesCache;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesCollect;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesLakeAndRiverNetworkGenerator;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesLandmassGenerator;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesMidpointDisplacement;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesRiverNetworkGenerator;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesVoronoiMountains;
import com.booleanbyte.specialization.worldsynth.module.heightmap.ModuleHeightmapFeaturesToHeightmap;
import com.booleanbyte.specialization.worldsynth.module.heightmap.ModuleHeightmapFeaturesToSdfHeightmap;
import com.booleanbyte.specialization.worldsynth.module.heightmap.ModuleHeightmapFeaturesToWaterDepth;
import com.booleanbyte.specialization.worldsynth.module.materialmap.ModuleMaterialmapFeaturesToMaterialmap;

import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class SpecializationModuleRegister extends AbstractModuleRegister {
	
	public SpecializationModuleRegister() {
		super();
		
		try {
			registerModule(ModuleFeaturesLandmassGenerator.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesRiverNetworkGenerator.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesMidpointDisplacement.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesLakeAndRiverNetworkGenerator.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesVoronoiMountains.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesCollect.class, "\\Specialization\\Features");
			registerModule(ModuleFeatureConflictRemoval.class, "\\Specialization\\Features");
			registerModule(ModuleFeaturesCache.class, "\\Specialization\\Features");
			
			registerModule(ModuleAttributesSimpleElevation.class, "\\Specialization\\Attributes");
			registerModule(ModuleAttributesSimpleMaterial.class, "\\Specialization\\Attributes");
			registerModule(ModuleAttributesSimpleWaterDepth.class, "\\Specialization\\Attributes");
			registerModule(ModuleAttributesCollect.class, "\\Specialization\\Attributes");
			
			registerModule(ModuleHeightmapFeaturesToHeightmap.class, "\\Specialization\\Heightmap");
			registerModule(ModuleHeightmapFeaturesToWaterDepth.class, "\\Specialization\\Heightmap");
			registerModule(ModuleHeightmapFeaturesToSdfHeightmap.class, "\\Specialization\\Heightmap");
			
			registerModule(ModuleMaterialmapFeaturesToMaterialmap.class, "\\Specialization\\Materialmap");
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
