package com.booleanbyte.specialization.worldsynth.module.heightmap;

import java.util.Map;

import com.booleanbyte.specialization.feature.Merger;
import com.booleanbyte.specialization.feature.ShapedFeature;
import com.booleanbyte.specialization.worldsynth.datatype.DatatypeFeatures;
import com.booleanbyte.specialization.worldsynth.module.features.ModuleFeaturesEvaluate;
import com.github.davidmoten.rtree2.Entry;
import com.github.davidmoten.rtree2.RTree;
import com.github.davidmoten.rtree2.geometry.Geometries;
import com.github.davidmoten.rtree2.geometry.Geometry;
import com.github.davidmoten.rtree2.geometry.Rectangle;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;

public class ModuleHeightmapFeaturesToWaterDepth extends ModuleFeaturesEvaluate {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		int grouping = this.grouping.getValue();
		boolean selective = this.selective.getValue();
		
		//Check if inputs are available
		if (inputs.get("features") == null) {
			//If there is not enough input just return null
			return null;
		}
		DatatypeFeatures featuresData = (DatatypeFeatures) inputs.get("features");
		
		//----------BUILD----------//
		
		System.out.println("Water depth");
		
		long t0 = System.currentTimeMillis();
		
		RTree<ShapedFeature, Geometry> rtree = createRtree(featuresData.getFeatures());
		
		long t1 = System.currentTimeMillis();
		System.out.println("RTree construct: Entries: " + featuresData.getFeatures().size() + " Time: " + (t1 - t0) + "ms");
		
		float[][] map = new float[mpw][mpl];
		
		long mergedFeaturesSum = 0;
		
		if (!selective) {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					Merger m = evaluateAt(x+u*res, z+v*res, featuresData.getFeatures()); // Evaluation without RTree search
					mergedFeaturesSum += m.mergedFeatures;
					map[u][v] = (float) m.getWaterDepth(); 
				}
			}
		}
		else if (grouping == 1) {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					Merger m = evaluateAt(x+u*res, z+v*res, rtree); // Evaluation with RTree point search
					mergedFeaturesSum += m.mergedFeatures;
					map[u][v] = (float) m.getWaterDepth(); 
				}
			}
		}
		else {
			// Grouped evaluation
			for (int u = 0; u < mpw; u+=grouping) {
				for (int v = 0; v < mpl; v+=grouping) {
					// Determine group rectangle
					double x1 = x + u*res;
					double z1 = z + v*res;
					double x2 = x1 + grouping*res;
					double z2 = z1 + grouping*res;
					Rectangle groupRectangle = Geometries.rectangle(x1, z1, x2, z2);
					
					// Find features who's influence may intersect the group rectangle
					Iterable<Entry<ShapedFeature, Geometry>> foundEntries = rtree.search(groupRectangle);
					
					// Evaluate the group
					for (int u_ = u; u_ < Math.min(mpw, u+grouping); u_++) {
						for (int v_ = v; v_ < Math.min(mpl, v+grouping); v_++) {
							Merger m = evaluateAt(x+u_*res, z+v_*res, foundEntries);
							mergedFeaturesSum += m.mergedFeatures;
							map[u_][v_] = (float) m.getWaterDepth();
						}
					}
				}
			}
		}
		
		long t2 = System.currentTimeMillis();
		System.out.println("Eval time: Total: " + (t2 - t1) + "ms Avg: " + (double) (t2 - t1)/(mpw * mpl) + "ms [" + mpw + ", " + mpl + "]");
		System.out.println("Eval features: Total: " + mergedFeaturesSum + " Avg: " + (double) mergedFeaturesSum / (mpw * mpl));
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Features to water depth";
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}
}
