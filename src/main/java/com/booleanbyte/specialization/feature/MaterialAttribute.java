package com.booleanbyte.specialization.feature;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import net.worldsynth.material.MaterialState;

public class MaterialAttribute implements Attribute {
	
	private final MaterialState<?, ?> material;
	private final WeightFunction weightFunction;
	
	public MaterialAttribute(MaterialState<?, ?> material, WeightFunction weightFunction) {
		this.material = material;
		this.weightFunction = weightFunction;
	}
	
	@Override
	public void contrubute(Merger merger, ShapedFeature feature, Vector2D p, double sdf) {
		double weight = weightFunction.apply(sdf);
		
		merger.contributeMaterial(material, weight);
	}
	
	@Override
	public double influenceDistance() {
		return weightFunction.influenceDistance();
	}
}
