package com.booleanbyte.specialization.feature;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public class ElevationAttribute implements Attribute {
	
	private final WeightFunction weightFunction;
	private final int priority;
	
	public ElevationAttribute(WeightFunction weightFunction, int priority) {
		this.weightFunction = weightFunction;
		this.priority = priority;
	}
	
	@Override
	public void contrubute(Merger merger, ShapedFeature feature, Vector2D p, double sdf) {
		double weight = weightFunction.apply(sdf);
		
		double height = feature.height(p);
		
		merger.contributeElevation(height, weight, priority);
	}
	
	@Override
	public double influenceDistance() {
		return weightFunction.influenceDistance();
	}
}
